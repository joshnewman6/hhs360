<h1>Heathfield High School 360 Degree Virtual Tour</h1>
<text>An introduction tour for Heathfield High School using 360-degree images and the [pannellum](https://pannellum.org/) API. <br> Available at https://hhs360.joshnewman6.com/.
<br>
<br>
<b>Limitations/Known Issues:</b>
<br>
- Images too large to work on some devices
<br>
- Sensitivity is quite high on touch screens


[ci]: https://about.gitlab.com/gitlab-ci/
[index.html]: https://gitlab.com/pages/plain-html/blob/master/public/index.html
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
